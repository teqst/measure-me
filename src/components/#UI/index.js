/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */
import PageLayout from "@/components/#UI/PageLayout";
import Modals from "@/components/#UI/Modals";
import Menu from "@/components/#UI/Menu";
import ButtonCard from "@/components/#UI/ButtonCard";

//Import components here as usual

export default [
    //Name of component
    PageLayout,
    Modals,
    Menu,
    ButtonCard
]