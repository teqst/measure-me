/**
 *
 *
 * @author Tatevosyan Artem (@teqst)
 */

//Import necessary objects

import firebase from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import 'firebase/firestore';

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
    apiKey: "AIzaSyADd_laLUDVMtCWDVoJU4q2IoHjcBGVdRs",
    authDomain: "measureme-3cca1.firebaseapp.com",
    projectId: "measureme-3cca1",
    storageBucket: "measureme-3cca1.appspot.com",
    messagingSenderId: "537041367658",
    appId: "1:537041367658:web:e7cd9e23084ef203301013",
    measurementId: "${config.measurementId}"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const timestamp = firebase.firestore.FieldValue.serverTimestamp;

export { timestamp };
export default firebaseApp.firestore();