import WaterCard from "./WaterCard";

export default {
    title: 'Components/Cards/WaterCard',
    component: WaterCard,
};

const Template = (args) => ({
    components: { WaterCard },
    setup() {
        return { args };
    },

    template: '<div style="margin: 25px;"><WaterCard v-bind="args"/></div>',
});

export const EnoughData = Template.bind({});
EnoughData.args = {
    title:"Water level",
    level: 74
};

export const noData = Template.bind({});
noData.args = {
    title:"Water level",
    level: undefined
};