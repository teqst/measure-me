import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import Health from "@/views/Health";
import Settings from "@/views/Settings";
import Support from "@/views/Support";

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/health',
    name: 'Health',
    component: Health
  },
  {
    path: '/settings',
    name: 'Settings',
    component: Settings
  },
  {
    path: '/support',
    name: 'Support',
    component: Support
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
