import { createApp } from 'vue'
import './registerServiceWorker'
import App from './App.vue'
import router from './router'
import store from './store'
import { library } from '@fortawesome/fontawesome-svg-core'
import components from '@/components/#UI'

import './assets/styles/global.css'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
   faHome,
   faNotesMedical,
   faRubleSign,
   faEllipsisH,
   faUserCircle,
   faPercent,
   faHeartbeat,
   faTint,
   faWaveSquare,
   faWeight,
   faEdit,
   faPlusCircle,
   faClinicMedical,
   faAngleRight
} from '@fortawesome/free-solid-svg-icons'

const app = createApp(App);

library.add(
    faWaveSquare,
    faHome,
    faNotesMedical,
    faRubleSign,
    faEllipsisH,
    faUserCircle,
    faPercent,
    faClinicMedical,
    faHeartbeat,
    faTint,
    faWeight,
    faEdit,
    faAngleRight,
    faPlusCircle,
)

components.forEach(component => {
   app.component(component.name, component);
});


app.component("font-awesome-icon", FontAwesomeIcon)
    .component("font-awesome-icon", FontAwesomeIcon)
    .use(store)
    .use(router)
    .mount('#app')
